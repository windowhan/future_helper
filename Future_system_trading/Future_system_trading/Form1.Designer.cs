﻿namespace Future_system_trading
{
    partial class command_form
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(command_form));
            this.axKFOpenAPI1 = new AxKFOpenAPILib.AxKFOpenAPI();
            this.debuglog_groupbox = new System.Windows.Forms.GroupBox();
            this.DebugLog_listBox2 = new System.Windows.Forms.ListBox();
            this.test_btn = new System.Windows.Forms.Button();
            this.DebugLog_listBox = new System.Windows.Forms.ListBox();
            this.today_center = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.today_open = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.current_price_text = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.max_trading_count = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.trading_count_limit_option = new System.Windows.Forms.CheckBox();
            this.trading_end_time = new System.Windows.Forms.TextBox();
            this.trade_start = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.chart_tick_count_text = new System.Windows.Forms.TextBox();
            this.trading_start_time = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.contract_mount_text = new System.Windows.Forms.TextBox();
            this.trading_time = new System.Windows.Forms.CheckBox();
            this.symbol_text = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.process1 = new System.Diagnostics.Process();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.current_trading_count = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.recent_time_text = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.MACD_Text = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.down_fractal_text = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.up_fractal_text = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.stop_loss_text = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.trailing_stop_text = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.var_update_btn = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.volatility_breakout_option_text = new System.Windows.Forms.TextBox();
            this.volatility_option = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.axKFOpenAPI1)).BeginInit();
            this.debuglog_groupbox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // axKFOpenAPI1
            // 
            this.axKFOpenAPI1.Enabled = true;
            this.axKFOpenAPI1.Location = new System.Drawing.Point(455, 141);
            this.axKFOpenAPI1.Name = "axKFOpenAPI1";
            this.axKFOpenAPI1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axKFOpenAPI1.OcxState")));
            this.axKFOpenAPI1.Size = new System.Drawing.Size(163, 45);
            this.axKFOpenAPI1.TabIndex = 0;
            this.axKFOpenAPI1.Visible = false;
            this.axKFOpenAPI1.OnReceiveTrData += new AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveTrDataEventHandler(this.axKFOpenAPI1_OnReceiveTrData);
            this.axKFOpenAPI1.OnReceiveMsg += new AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveMsgEventHandler(this.axKFOpenAPI1_OnReceiveMsg);
            this.axKFOpenAPI1.OnReceiveRealData += new AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveRealDataEventHandler(this.axKFOpenAPI1_OnReceiveRealData);
            this.axKFOpenAPI1.OnReceiveChejanData += new AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveChejanDataEventHandler(this.axKFOpenAPI1_OnReceiveChejanData);
            this.axKFOpenAPI1.OnEventConnect += new AxKFOpenAPILib._DKFOpenAPIEvents_OnEventConnectEventHandler(this.axKFOpenAPI1_OnEventConnect);
            // 
            // debuglog_groupbox
            // 
            this.debuglog_groupbox.Controls.Add(this.DebugLog_listBox2);
            this.debuglog_groupbox.Controls.Add(this.test_btn);
            this.debuglog_groupbox.Controls.Add(this.DebugLog_listBox);
            this.debuglog_groupbox.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.debuglog_groupbox.Location = new System.Drawing.Point(12, 281);
            this.debuglog_groupbox.Name = "debuglog_groupbox";
            this.debuglog_groupbox.Size = new System.Drawing.Size(1001, 309);
            this.debuglog_groupbox.TabIndex = 2;
            this.debuglog_groupbox.TabStop = false;
            this.debuglog_groupbox.Text = "디버깅 로그";
            this.debuglog_groupbox.Enter += new System.EventHandler(this.debuglog_groupbox_Enter);
            // 
            // DebugLog_listBox2
            // 
            this.DebugLog_listBox2.FormattingEnabled = true;
            this.DebugLog_listBox2.HorizontalScrollbar = true;
            this.DebugLog_listBox2.ItemHeight = 17;
            this.DebugLog_listBox2.Location = new System.Drawing.Point(480, 28);
            this.DebugLog_listBox2.Name = "DebugLog_listBox2";
            this.DebugLog_listBox2.Size = new System.Drawing.Size(515, 242);
            this.DebugLog_listBox2.TabIndex = 16;
            // 
            // test_btn
            // 
            this.test_btn.Location = new System.Drawing.Point(861, 276);
            this.test_btn.Name = "test_btn";
            this.test_btn.Size = new System.Drawing.Size(134, 27);
            this.test_btn.TabIndex = 15;
            this.test_btn.Text = "기능 테스트";
            this.test_btn.UseVisualStyleBackColor = true;
            this.test_btn.Click += new System.EventHandler(this.test_btn_Click);
            // 
            // DebugLog_listBox
            // 
            this.DebugLog_listBox.FormattingEnabled = true;
            this.DebugLog_listBox.ItemHeight = 17;
            this.DebugLog_listBox.Location = new System.Drawing.Point(10, 28);
            this.DebugLog_listBox.Name = "DebugLog_listBox";
            this.DebugLog_listBox.Size = new System.Drawing.Size(464, 242);
            this.DebugLog_listBox.TabIndex = 1;
            // 
            // today_center
            // 
            this.today_center.Location = new System.Drawing.Point(108, 44);
            this.today_center.Name = "today_center";
            this.today_center.Size = new System.Drawing.Size(135, 23);
            this.today_center.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 15);
            this.label5.TabIndex = 18;
            this.label5.Text = "당일 중심선";
            // 
            // today_open
            // 
            this.today_open.Location = new System.Drawing.Point(108, 228);
            this.today_open.Name = "today_open";
            this.today_open.Size = new System.Drawing.Size(135, 23);
            this.today_open.TabIndex = 17;
            this.today_open.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 232);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 15);
            this.label4.TabIndex = 16;
            this.label4.Text = "당일 시가";
            // 
            // current_price_text
            // 
            this.current_price_text.Location = new System.Drawing.Point(108, 19);
            this.current_price_text.Name = "current_price_text";
            this.current_price_text.Size = new System.Drawing.Size(135, 23);
            this.current_price_text.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(51, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 15);
            this.label7.TabIndex = 13;
            this.label7.Text = "현재가";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.volatility_option);
            this.groupBox2.Controls.Add(this.max_trading_count);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.trading_count_limit_option);
            this.groupBox2.Controls.Add(this.trading_end_time);
            this.groupBox2.Controls.Add(this.trade_start);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.chart_tick_count_text);
            this.groupBox2.Controls.Add(this.trading_start_time);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.contract_mount_text);
            this.groupBox2.Controls.Add(this.trading_time);
            this.groupBox2.Controls.Add(this.symbol_text);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(384, 263);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "설정";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // max_trading_count
            // 
            this.max_trading_count.Location = new System.Drawing.Point(270, 150);
            this.max_trading_count.Name = "max_trading_count";
            this.max_trading_count.Size = new System.Drawing.Size(62, 23);
            this.max_trading_count.TabIndex = 35;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(205, 153);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 15);
            this.label13.TabIndex = 34;
            this.label13.Text = "최대 회수";
            // 
            // trading_count_limit_option
            // 
            this.trading_count_limit_option.AutoSize = true;
            this.trading_count_limit_option.Location = new System.Drawing.Point(198, 125);
            this.trading_count_limit_option.Name = "trading_count_limit_option";
            this.trading_count_limit_option.Size = new System.Drawing.Size(158, 19);
            this.trading_count_limit_option.TabIndex = 33;
            this.trading_count_limit_option.Text = "트레이딩 횟수 제한 옵션";
            this.trading_count_limit_option.UseVisualStyleBackColor = true;
            // 
            // trading_end_time
            // 
            this.trading_end_time.Location = new System.Drawing.Point(93, 177);
            this.trading_end_time.Name = "trading_end_time";
            this.trading_end_time.Size = new System.Drawing.Size(62, 23);
            this.trading_end_time.TabIndex = 32;
            // 
            // trade_start
            // 
            this.trade_start.Location = new System.Drawing.Point(19, 222);
            this.trade_start.Name = "trade_start";
            this.trade_start.Size = new System.Drawing.Size(341, 35);
            this.trade_start.TabIndex = 6;
            this.trade_start.Text = "시작";
            this.trade_start.UseVisualStyleBackColor = true;
            this.trade_start.Click += new System.EventHandler(this.trade_start_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 180);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 15);
            this.label12.TabIndex = 31;
            this.label12.Text = "종료시간";
            // 
            // chart_tick_count_text
            // 
            this.chart_tick_count_text.Location = new System.Drawing.Point(93, 80);
            this.chart_tick_count_text.Name = "chart_tick_count_text";
            this.chart_tick_count_text.Size = new System.Drawing.Size(263, 23);
            this.chart_tick_count_text.TabIndex = 5;
            this.chart_tick_count_text.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // trading_start_time
            // 
            this.trading_start_time.Location = new System.Drawing.Point(93, 150);
            this.trading_start_time.Name = "trading_start_time";
            this.trading_start_time.Size = new System.Drawing.Size(62, 23);
            this.trading_start_time.TabIndex = 30;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 153);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 15);
            this.label11.TabIndex = 29;
            this.label11.Text = "시작시간";
            // 
            // contract_mount_text
            // 
            this.contract_mount_text.Location = new System.Drawing.Point(93, 51);
            this.contract_mount_text.Name = "contract_mount_text";
            this.contract_mount_text.Size = new System.Drawing.Size(263, 23);
            this.contract_mount_text.TabIndex = 4;
            // 
            // trading_time
            // 
            this.trading_time.AutoSize = true;
            this.trading_time.Location = new System.Drawing.Point(15, 125);
            this.trading_time.Name = "trading_time";
            this.trading_time.Size = new System.Drawing.Size(158, 19);
            this.trading_time.TabIndex = 28;
            this.trading_time.Text = "트레이딩 타임 제한 옵션";
            this.trading_time.UseVisualStyleBackColor = true;
            // 
            // symbol_text
            // 
            this.symbol_text.Location = new System.Drawing.Point(93, 22);
            this.symbol_text.Name = "symbol_text";
            this.symbol_text.Size = new System.Drawing.Size(263, 23);
            this.symbol_text.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "틱차트 틱값";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "종목코드";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "계약수";
            // 
            // process1
            // 
            this.process1.StartInfo.Domain = "";
            this.process1.StartInfo.LoadUserProfile = false;
            this.process1.StartInfo.Password = null;
            this.process1.StartInfo.StandardErrorEncoding = null;
            this.process1.StartInfo.StandardOutputEncoding = null;
            this.process1.StartInfo.UserName = "";
            this.process1.SynchronizingObject = this;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.current_trading_count);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.recent_time_text);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.MACD_Text);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.down_fractal_text);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.up_fractal_text);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.today_center);
            this.groupBox1.Controls.Add(this.today_open);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.current_price_text);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.groupBox1.Location = new System.Drawing.Point(402, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(267, 257);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "주요 변수";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // current_trading_count
            // 
            this.current_trading_count.Location = new System.Drawing.Point(108, 85);
            this.current_trading_count.Name = "current_trading_count";
            this.current_trading_count.Size = new System.Drawing.Size(135, 23);
            this.current_trading_count.TabIndex = 29;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 88);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 15);
            this.label14.TabIndex = 28;
            this.label14.Text = "현재 거래횟수";
            // 
            // recent_time_text
            // 
            this.recent_time_text.Location = new System.Drawing.Point(108, 143);
            this.recent_time_text.Name = "recent_time_text";
            this.recent_time_text.Size = new System.Drawing.Size(135, 23);
            this.recent_time_text.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 146);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 15);
            this.label10.TabIndex = 26;
            this.label10.Text = "업데이트 시간";
            // 
            // MACD_Text
            // 
            this.MACD_Text.Location = new System.Drawing.Point(108, 114);
            this.MACD_Text.Name = "MACD_Text";
            this.MACD_Text.Size = new System.Drawing.Size(135, 23);
            this.MACD_Text.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(35, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 15);
            this.label9.TabIndex = 24;
            this.label9.Text = "MACD 값";
            // 
            // down_fractal_text
            // 
            this.down_fractal_text.Location = new System.Drawing.Point(108, 196);
            this.down_fractal_text.Name = "down_fractal_text";
            this.down_fractal_text.Size = new System.Drawing.Size(135, 23);
            this.down_fractal_text.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 15);
            this.label8.TabIndex = 22;
            this.label8.Text = "하단 프랙탈";
            // 
            // up_fractal_text
            // 
            this.up_fractal_text.Location = new System.Drawing.Point(108, 169);
            this.up_fractal_text.Name = "up_fractal_text";
            this.up_fractal_text.Size = new System.Drawing.Size(135, 23);
            this.up_fractal_text.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 177);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 15);
            this.label6.TabIndex = 20;
            this.label6.Text = "상단 프랙탈";
            // 
            // timer1
            // 
            this.timer1.Interval = 600;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.volatility_breakout_option_text);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.var_update_btn);
            this.groupBox3.Controls.Add(this.trailing_stop_text);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.stop_loss_text);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.groupBox3.Location = new System.Drawing.Point(675, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(332, 263);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "수정 가능 변수";
            // 
            // stop_loss_text
            // 
            this.stop_loss_text.Location = new System.Drawing.Point(104, 23);
            this.stop_loss_text.Name = "stop_loss_text";
            this.stop_loss_text.Size = new System.Drawing.Size(94, 23);
            this.stop_loss_text.TabIndex = 14;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(43, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 15);
            this.label15.TabIndex = 15;
            this.label15.Text = "스탑로스";
            // 
            // trailing_stop_text
            // 
            this.trailing_stop_text.Location = new System.Drawing.Point(104, 51);
            this.trailing_stop_text.Name = "trailing_stop_text";
            this.trailing_stop_text.Size = new System.Drawing.Size(94, 23);
            this.trailing_stop_text.TabIndex = 16;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 54);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 15);
            this.label16.TabIndex = 17;
            this.label16.Text = "트레일링 스탑";
            // 
            // var_update_btn
            // 
            this.var_update_btn.Location = new System.Drawing.Point(6, 221);
            this.var_update_btn.Name = "var_update_btn";
            this.var_update_btn.Size = new System.Drawing.Size(320, 35);
            this.var_update_btn.TabIndex = 36;
            this.var_update_btn.Text = "수정";
            this.var_update_btn.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 83);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(95, 15);
            this.label17.TabIndex = 37;
            this.label17.Text = "변동성 돌파여부";
            // 
            // volatility_breakout_option_text
            // 
            this.volatility_breakout_option_text.Location = new System.Drawing.Point(104, 80);
            this.volatility_breakout_option_text.Name = "volatility_breakout_option_text";
            this.volatility_breakout_option_text.Size = new System.Drawing.Size(94, 23);
            this.volatility_breakout_option_text.TabIndex = 38;
            // 
            // volatility_option
            // 
            this.volatility_option.AutoSize = true;
            this.volatility_option.Location = new System.Drawing.Point(198, 181);
            this.volatility_option.Name = "volatility_option";
            this.volatility_option.Size = new System.Drawing.Size(154, 19);
            this.volatility_option.TabIndex = 36;
            this.volatility_option.Text = "변동성 돌파시에만 매매";
            this.volatility_option.UseVisualStyleBackColor = true;
            //this.volatility_option.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // command_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 602);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.axKFOpenAPI1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.debuglog_groupbox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "command_form";
            this.Text = "Future Trading Helper";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.command_form_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.axKFOpenAPI1)).EndInit();
            this.debuglog_groupbox.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private AxKFOpenAPILib.AxKFOpenAPI axKFOpenAPI1;
        private System.Windows.Forms.GroupBox debuglog_groupbox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox DebugLog_listBox;
        private System.Windows.Forms.Button trade_start;
        private System.Windows.Forms.TextBox chart_tick_count_text;
        private System.Windows.Forms.TextBox contract_mount_text;
        private System.Windows.Forms.TextBox symbol_text;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox current_price_text;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button test_btn;
        private System.Windows.Forms.TextBox today_center;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox today_open;
        private System.Windows.Forms.Label label4;
        private System.Diagnostics.Process process1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox MACD_Text;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox down_fractal_text;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox up_fractal_text;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox recent_time_text;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ListBox DebugLog_listBox2;
        private System.Windows.Forms.TextBox trading_end_time;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox trading_start_time;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox trading_time;
        private System.Windows.Forms.TextBox max_trading_count;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox trading_count_limit_option;
        private System.Windows.Forms.TextBox current_trading_count;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox volatility_option;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox volatility_breakout_option_text;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button var_update_btn;
        private System.Windows.Forms.TextBox trailing_stop_text;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox stop_loss_text;
        private System.Windows.Forms.Label label15;
    }
}


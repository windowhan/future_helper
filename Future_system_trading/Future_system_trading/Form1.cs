﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
//using MySql.Data.MySqlClient;


namespace Future_system_trading
{

    public partial class command_form : Form
    {
        string account_number;
        bool login_flag;

        // MYSQL 연결 정보
        //string server_addr;
        //string database_name;
        //string user_name;
        //string password;
        //string strConn;
        //MySqlConnection conn;
        static double current_price;

        List<double> open_array;
        List<double> close_array;
        List<double> high_array;
        List<double> low_array;

        double day_open = 0;
        double day_center_line = 0;
        string raw_recent_macd_value = "";
        double current_macd_value = -1000;
        double prev_macd_value = -1000;
        double stop_loss = 0;
        int trading_count = 0;

        int active_buy_pos_count = 0;
        int active_sell_pos_count = 0;

        // 0 - No position
        // 1 - Long 
        // 2 - Short
        int trade_status = 0;

        // 실시간 시세/호가 F
        public command_form()
        {
            InitializeComponent();
            login_flag = false;
            //server_addr = "45.76.215.119";

            open_array = new List<double>();
            close_array = new List<double>();
            high_array = new List<double>();
            low_array = new List<double>();

            /*database_name = "cme_future_trading";
            user_name = "root";
            password = "dnflwlq6861!";
            strConn = "Server=" + server_addr + ";Database=" + database_name + ";Uid=" + user_name + ";Pwd=" + password + ";";
            conn = new MySqlConnection(strConn);
            conn.Open();
            */
            account_number = "";
            axKFOpenAPI1.CommConnect(1);
        }

        public bool isTradingTime()
        {
            bool result = true;
            DateTime current_time = DateTime.Now;

            int start_time = int.Parse(trading_start_time.Text);
            int end_time = int.Parse(trading_end_time.Text);

            if (trading_time.Checked == true)
            {
                if ((start_time > end_time) && ((current_time.Hour > start_time && current_time.Hour > end_time) || (current_time.Hour < start_time && current_time.Hour < end_time)))
                {
                    result = true;
                }

                else if ((start_time < end_time) && ((current_time.Hour > start_time && current_time.Hour < end_time)))
                {
                    result = true;
                }

                else
                    result = false;
            }
            return result;
        }

        public bool IsError(int iErrCode)
        {
            string strMsg = "";
            switch (iErrCode)
            {
                case Constants.OP_ERR_NO_LOGIN:
                    strMsg = "미접속상태";
                    break;
                case Constants.OP_ERR_TRCODE:
                    strMsg = "TrCode가 존재하지 않습니다.";
                    break;
                case Constants.OP_ERR_SISE_OVERFLOW:
                    strMsg = "시세조회 과부하";
                    break;
                case Constants.OP_ERR_ORDER_OVERFLOW:
                    strMsg = "주문 과부하";
                    break;
                case Constants.OP_ERR_RQ_WRONG_INPUT:
                    strMsg = "조회입력값 오류";
                    break;
                case Constants.OP_ERR_ORD_WRONG_INPUT:
                    strMsg = "주문입력값 오류";
                    break;
                case Constants.OP_ERR_ORD_WRONG_ACCPWD:
                    strMsg = "계좌비밀번호를 입력하십시오.";
                    break;
                case Constants.OP_ERR_ORD_WRONG_ACCNO:
                    strMsg = "타인 계좌를 사용할 수 없습니다.";
                    break;
                case Constants.OP_ERR_ORD_WRONG_QTY200:
                    strMsg = "경고-주문수량 200개 초과";
                    break;
                case Constants.OP_ERR_ORD_WRONG_QTY400:
                    strMsg = "제한-주문수량 400개 초과";
                    break;
            }

            // 에러 메세지 처리
            if (strMsg != "")
            {
                MessageBox.Show(strMsg);
                return false;
            }

            return true;
        }

        private void axKFOpenAPI1_OnEventConnect(object sender, AxKFOpenAPILib._DKFOpenAPIEvents_OnEventConnectEvent e)
        {
            if (e.nErrCode == 0)
            {
                if (this.axKFOpenAPI1.GetConnectState() == 1)
                {
                    /*
                     *   BSTR sTag에 들어 갈 수 있는 값은 아래와 같음
                     *   “ACCOUNT_CNT” – 전체 계좌 개수를 반환한다.
                     *   "ACCNO" – 전체 계좌를 반환한다. 계좌별 구분은 ‘;’이다.
                     *   “USER_ID” - 사용자 ID를 반환한다. 
                     */

                    ThreadStart ts = new ThreadStart(connect_check);
                    Thread t = new Thread(ts);
                    t.Start();

                    login_flag = true;
                    this.Visible = true;
                    this.Opacity = 100;
                    DebugLog_listBox.Items.Add("로그인 성공..");
                    DebugLog_listBox.Items.Add("연결중인 상태입니다.");

                    account_number = this.axKFOpenAPI1.GetLoginInfo("ACCNO").Replace(";", "");
                    DebugLog_listBox.Items.Add("계좌 번호 : " + account_number);
                    //debuglog_text.Text += "현재 해외선물 종목코드 : " + this.axKFOpenAPI1.GetGlobalFutureCodelist("CL") + "\r\n";
                }
            }

            else
            {
                //MessageBox.Show("로그인 실패");
            }
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            this.Visible = false;
            this.Opacity = 0;
        }

        public void connect_check()
        {
            //debuglog_text.Text += "로그인 정보 : " + this.axKFOpenAPI1.GetLoginInfo("ACCOUNT_CNT") + "\r\n";
            while (true)
            {
                if (this.axKFOpenAPI1.GetConnectState() != 1 && login_flag == true)
                {
                    // 재연결 (자동 재연결 하는 방법이 딱히 없는거같음)
                    //debuglog_text.Text += "연결 끊김... " + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + "\r\n";
                    //MessageBox.Show("재연결 시도");
                    axKFOpenAPI1.CommConnect(1);
                    break;
                }
            }
        }


        private void axKFOpenAPI1_OnReceiveRealData(object sender, AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            if (e.sRealType.Trim() == "해외선물시세" || e.sRealType.Trim() == "해외옵션시세")
            {
                current_price = double.Parse(axKFOpenAPI1.GetCommRealData(e.sRealType, 140));
                current_price_text.Text = current_price.ToString();

                // 손절 및 트레일링 스탑
                // 롱포지션일때
                if (trade_status == 1)
                {
                    if (current_price < stop_loss || current_price < down_fractal)
                    {
                        int ret = axKFOpenAPI1.SendOrder("선물옵션현재가", "1000", this.account_number, 1, symbol_text.Text, int.Parse(contract_mount_text.Text), "0", "0", "1", "");
                        DebugLog_listBox2.Items.Add("실시간 매수 청산 완료");
                        DebugLog_listBox2.Items.Add("현 가격 : " + current_price.ToString() + " | 하단프랙탈 : " + down_fractal.ToString() + " | 스탑로스 : " + stop_loss.ToString());
                        DebugLog_listBox2.Items.Add("리턴 값 : " + ret);
                        trade_status = 0;
                    }
                }

                // 숏포지션일때
                else if (trade_status == 2)
                {
                    if (current_price > stop_loss || current_price > up_fractal)
                    {
                        int ret = axKFOpenAPI1.SendOrder("선물옵션현재가", "1000", this.account_number, 2, symbol_text.Text, int.Parse(contract_mount_text.Text), "0", "0", "1", "");
                        DebugLog_listBox2.Items.Add("실시간 매도 청산 완료");
                        DebugLog_listBox2.Items.Add("현 가격 : " + current_price.ToString() + " | 상단프랙탈 : " + up_fractal.ToString() + " | 스탑로스 : " + stop_loss.ToString());
                        DebugLog_listBox2.Items.Add("리턴 값 : " + ret);
                        trade_status = 0;
                    }
                }

            }
        }

        private void get_tick_start_button_Click(object sender, EventArgs e)
        {
            /*string code = add_code_text.Text ;
            string RQName = "선물옵션현재가";
            string TRCode = Constants.TR_OPT10001; // 현재가
        
            this.axKFOpenAPI1.SetInputValue("종목코드", code);
            string i_dont_know = "0001";
            int iRet = axKFOpenAPI1.CommRqData(RQName, TRCode, "", i_dont_know);

            string strErr;
            if (IsError(iRet))
            {
                strErr = string.Format("조회요청 에러 [{0:s}][{0:d}]", TRCode, iRet);
                //debuglog_text.Text += strErr + "\r\n";
            }*/
        }


        double first_high, second_high, third_high, fourth_high, fifth_high = 0;
        double first_low, second_low, third_low, fourth_low, fifth_low = 0;
        double up_fractal, down_fractal = 0;

        public double MaxDoubleArrayMember(List<double> list_var, int start, int end)
        {
            double max = list_var[start];
            for (int i = start; i < end; i++)
            {
                if (list_var[i] > max)
                    max = list_var[i];
            }

            return max;
        }

        public double MinDoubleArrayMember(List<double> list_var, int start, int end)
        {
            double min = list_var[start];
            for (int i = start; i < end; i++)
            {
                if (list_var[i] < min)
                    min = list_var[i];
            }

            return min;
        }

        private void axKFOpenAPI1_OnReceiveTrData(object sender, AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            string output = string.Empty;
            //int short_term_sma = Int32.Parse(Short_Term_SMA_Period.Text);
            //int long_term_sma = Int32.Parse(Long_Term_SMA_Period.Text);

            double open = 0, close = 0, high = 0, low = 0;
            int ret = 0;
            double etc_standard_value = 0;
            string[] raw_macd_array;
            string raw_str = "";

            if (e.sRQName == "해외선물옵션틱차트조회")
            {
                int result = axKFOpenAPI1.GetRepeatCnt(e.sTrCode, e.sRQName);
                //DebugLog_listBox.Items.Add("가져올 수 있는 틱봉 개수 : " + result.ToString());

                //double dayopen = 0;
                string time = string.Empty;
                for (int i = 30; i > 0; i--)
                {
                    // 캔들 정보
                    time = axKFOpenAPI1.GetCommData(e.sTrCode, e.sRQName, i, "체결시간");

                    close = double.Parse(axKFOpenAPI1.GetCommData(e.sTrCode, e.sRQName, i, "현재가"));
                    open = double.Parse(axKFOpenAPI1.GetCommData(e.sTrCode, e.sRQName, i, "시가"));
                    low = double.Parse(axKFOpenAPI1.GetCommData(e.sTrCode, e.sRQName, i, "저가"));
                    high = double.Parse(axKFOpenAPI1.GetCommData(e.sTrCode, e.sRQName, i, "고가"));

                    close_array.Add(close);
                    open_array.Add(open);
                    high_array.Add(high);
                    low_array.Add(low);


                    // 프랙탈 상태
                    int fractal_n = 2;
                    // if((high(n-2) < high(n)) and (high(n-1) < high(n)) and (high(n+1) < high(n)) and (high(n+2) < high(n)), high(n), -5)
                    // if((low(n-2) > low(n)) and (low(n-1) > low(n)) and (low(n+1) > low(n)) and (low(n+2) > low(n)), low(n), -5)

                    if ((high_array.Count - 1 - fractal_n + 2) > 0 && (high_array.Count - 1 - fractal_n - 2) > 0)
                    {
                        first_high = high_array[high_array.Count - 1 - fractal_n + 2]; // high(n-2)
                        second_high = high_array[high_array.Count - 1 - fractal_n]; // high(n)
                        third_high = high_array[high_array.Count - 1 - fractal_n + 1]; // high(n-1)
                        fourth_high = high_array[high_array.Count - 1 - fractal_n - 1]; // high(n+1)
                        fifth_high = high_array[high_array.Count - 1 - fractal_n - 2]; // high(n+2)
                        if ((first_high < second_high) && (third_high < second_high) && (fourth_high < second_high) && (fifth_high < second_high))
                        {
                            output += " | 상단 프랙탈(" + second_high + ")";
                            up_fractal = second_high;
                        }
                    }

                    if ((low_array.Count - 1 - fractal_n + 2) > 0 && (low_array.Count - 1 - fractal_n - 2) > 0)
                    {
                        first_low = low_array[low_array.Count - 1 - fractal_n + 2]; // low(n-2)
                        second_low = low_array[low_array.Count - 1 - fractal_n]; // low(n)
                        third_low = low_array[low_array.Count - 1 - fractal_n + 1]; // low(n-1)
                        fourth_low = low_array[low_array.Count - 1 - fractal_n - 1]; // low(n+1)
                        fifth_low = low_array[low_array.Count - 1 - fractal_n - 2]; // low(n+2)
                        if ((first_low > second_low) && (third_low > second_low) && (fourth_low > second_low) && (fifth_low > second_low))
                        {
                            output += " | 하단 프랙탈(" + second_low + ")";
                            down_fractal = second_low;
                        }
                    }


                    //DebugLog_listBox.Items.Add(output);
                }

                try
                {
                    raw_str = axKFOpenAPI1.GetCommonFunc("GetMACD", "1000;틱차트;0;12;26;9").ToString();
                }
                catch (AccessViolationException ex)
                {
                    DebugLog_listBox.Items.Add(ex.Message);
                    return;
                }


                raw_macd_array = Regex.Split(raw_str, "                   ");

                if (String.Compare(raw_recent_macd_value, raw_macd_array[raw_macd_array.Length - 3]) != 0)
                {
                    prev_macd_value = current_macd_value;
                    raw_recent_macd_value = raw_macd_array[raw_macd_array.Length - 3];

                    current_macd_value = double.Parse(raw_recent_macd_value.ToString().Replace(" ", "").Substring(0, 6));
                    MACD_Text.Text = current_macd_value.ToString();
                    recent_time_text.Text = raw_recent_macd_value.ToString().Replace(" ", "").Substring(6, raw_recent_macd_value.ToString().Replace(" ", "").Length - 6);
                    up_fractal_text.Text = up_fractal.ToString();
                    down_fractal_text.Text = down_fractal.ToString();

                    output = "업데이트 체결시간(" + time + ")" + output;
                    output += " | 고가(" + high.ToString() + ")";
                    output += " | 저가(" + low.ToString() + ")";
                    output += " | 시가(" + open.ToString() + ")";
                    output += " | 종가(" + close.ToString() + ")";

                    DebugLog_listBox2.Items.Add(output);
                    DebugLog_listBox2.Items.Add("MACD Raw 데이터 업데이트 : " + raw_recent_macd_value);
                    DebugLog_listBox2.Items.Add("prev_macd_value : " + prev_macd_value.ToString());
                    DebugLog_listBox2.Items.Add("current_macd_value : " + current_macd_value.ToString());

                    if (isTradingTime() == false)
                        return;

                    // 시가, 중심선 retouch하고 안쪽으로 올때 다시 손절
                    if (trade_status == 1)
                    {
                        if (day_center_line > day_open)
                            etc_standard_value = day_center_line;
                        else
                            etc_standard_value = day_open;

                        if (close < etc_standard_value)
                        {
                            // 롱포지션 손절
                            ret = axKFOpenAPI1.SendOrder("선물옵션현재가", "1000", this.account_number, 1, symbol_text.Text, int.Parse(contract_mount_text.Text), "0", "0", "1", "");
                            DebugLog_listBox2.Items.Add("실시간 매수 청산 완료");
                            DebugLog_listBox2.Items.Add("현 가격 : " + current_price.ToString() + " | 하단프랙탈 : " + down_fractal.ToString() + " | 스탑로스 : " + stop_loss.ToString());
                            DebugLog_listBox2.Items.Add("리턴 값 : " + ret);
                            trade_status = 0;
                        }
                    }

                    else if (trade_status == 2)
                    {
                        if (day_center_line < day_open)
                            etc_standard_value = day_center_line;
                        else
                            etc_standard_value = day_open;

                        if (close > etc_standard_value)
                        {
                            // 숏포지션 손절
                            ret = axKFOpenAPI1.SendOrder("선물옵션현재가", "1000", this.account_number, 2, symbol_text.Text, int.Parse(contract_mount_text.Text), "0", "0", "1", "");
                            DebugLog_listBox2.Items.Add("실시간 매도 청산 완료");
                            DebugLog_listBox2.Items.Add("현 가격 : " + current_price.ToString() + " | 상단프랙탈 : " + up_fractal.ToString() + " | 스탑로스 : " + stop_loss.ToString());
                            DebugLog_listBox2.Items.Add("리턴 값 : " + ret);
                            trade_status = 0;
                        }
                    }

                    // 강세 때 시가, 중심선 뚫는 움직임 나왔을 때 
                    if (current_macd_value > 0)
                    {
                        if (day_center_line > day_open)
                            etc_standard_value = day_center_line;
                        else
                            etc_standard_value = day_open;

                        if (trade_status == 0 && open < etc_standard_value && close > etc_standard_value && close > down_fractal && (trading_count_limit_option.Checked == false || (trading_count_limit_option.Checked == true && trading_count < int.Parse(max_trading_count.Text))))
                        {
                            DebugLog_listBox2.Items.Add("매수조건2 충족");
                            trade_status = 1;
                            stop_loss = MinDoubleArrayMember(low_array, low_array.Count - 4, low_array.Count - 2);
                            //stop_loss = low_array[low_array.Count - 4];
                            active_buy_pos_count = Int32.Parse(contract_mount_text.Text);
                            trading_count++;

                            ret = axKFOpenAPI1.SendOrder("선물옵션현재가", "1000", this.account_number, 2, symbol_text.Text, int.Parse(contract_mount_text.Text), "0", "0", "1", "");
                            DebugLog_listBox2.Items.Add("체결 결과 값 : " + ret.ToString());
                            current_trading_count.Text = trading_count.ToString();
                        }
                    }
                    // 약세 때 시가, 중심선 뚫는 움직임 나왔을 때
                    else if (current_macd_value < 0)
                    {
                        if (day_center_line < day_open)
                            etc_standard_value = day_center_line;
                        else
                            etc_standard_value = day_open;

                        if (trade_status == 0 && open > etc_standard_value && close < etc_standard_value && close < up_fractal && (trading_count_limit_option.Checked == false || (trading_count_limit_option.Checked == true && trading_count < int.Parse(max_trading_count.Text))))
                        {
                            DebugLog_listBox2.Items.Add("매도조건2 충족");
                            trade_status = 2;
                            stop_loss = MaxDoubleArrayMember(high_array, high_array.Count - 4, high_array.Count - 2);
                            //stop_loss = high_array[high_array.Count - 4];
                            active_sell_pos_count = Int32.Parse(contract_mount_text.Text);
                            trading_count++;

                            ret = axKFOpenAPI1.SendOrder("선물옵션현재가", "1000", this.account_number, 1, symbol_text.Text, int.Parse(contract_mount_text.Text), "0", "0", "1", "");
                            DebugLog_listBox2.Items.Add("체결 결과 값 : " + ret.ToString());
                            current_trading_count.Text = trading_count.ToString();
                        }
                    }

                    // 강세 반전
                    if (((prev_macd_value != -1000 && prev_macd_value < 0) || (prev_macd_value != -1000 && prev_macd_value == 0)) && (current_macd_value > 0))
                    {
                        // 반대 포지션 청산 코드 필요
                        if (trade_status != 0)
                            ret = axKFOpenAPI1.SendOrder("선물옵션현재가", "1000", this.account_number, 1, symbol_text.Text, active_buy_pos_count, "0", "0", "1", "");
                        //
                        DebugLog_listBox2.Items.Add("MACD 강세반전 출현");
                        if ((close > day_center_line) && (close > day_open) && (trading_count_limit_option.Checked == false || (trading_count_limit_option.Checked == true && trading_count < int.Parse(max_trading_count.Text))))
                        {
                            DebugLog_listBox2.Items.Add("매수 조건 충족");
                            ret = axKFOpenAPI1.SendOrder("선물옵션현재가", "1000", this.account_number, 2, symbol_text.Text, int.Parse(contract_mount_text.Text), "0", "0", "1", "");
                            DebugLog_listBox2.Items.Add("체결 결과 값 : " + ret.ToString());
                            trade_status = 1;
                            trading_count++;

                            stop_loss = MinDoubleArrayMember(low_array, low_array.Count - 4, low_array.Count - 2);
                            //stop_loss = low_array[low_array.Count - 4];
                            active_buy_pos_count = Int32.Parse(contract_mount_text.Text);
                            current_trading_count.Text = trading_count.ToString();
                        }
                    }

                    // 약세 반전
                    else if (((prev_macd_value != -1000 && prev_macd_value > 0) || (prev_macd_value != -1000 && prev_macd_value == 0)) && (current_macd_value < 0))
                    {
                        // 반대 포지션 청산 코드 필요
                        if (trade_status != 0)
                            ret = axKFOpenAPI1.SendOrder("선물옵션현재가", "1000", this.account_number, 2, symbol_text.Text, active_sell_pos_count, "0", "0", "1", "");
                        //

                        DebugLog_listBox2.Items.Add("MACD 약세반전 출현");
                        if ((close < day_center_line) && (close < day_open) && (trading_count_limit_option.Checked == false || (trading_count_limit_option.Checked == true && trading_count < int.Parse(max_trading_count.Text))))
                        {
                            DebugLog_listBox2.Items.Add("매도 조건 충족");
                            ret = axKFOpenAPI1.SendOrder("선물옵션현재가", "1000", this.account_number, 1, symbol_text.Text, int.Parse(contract_mount_text.Text), "0", "0", "1", "");
                            DebugLog_listBox2.Items.Add("체결 결과 값 : " + ret.ToString());
                            trading_count++;

                            trade_status = 2;
                            stop_loss = MaxDoubleArrayMember(high_array, high_array.Count - 4, high_array.Count - 2);
                            //stop_loss = high_array[high_array.Count - 4];
                            active_sell_pos_count = Int32.Parse(contract_mount_text.Text);
                            current_trading_count.Text = trading_count.ToString();
                        }
                    }

                    output = "";
                }


            }

            else if (e.sRQName == "해외선물옵션일차트조회")
            {
                int result = axKFOpenAPI1.GetRepeatCnt(e.sTrCode, e.sRQName);
                day_open = Double.Parse(axKFOpenAPI1.GetCommData(e.sTrCode, e.sRQName, 0, "시가"));
                day_center_line = Double.Parse(axKFOpenAPI1.GetCommData(e.sTrCode, e.sRQName, 0, "저가")) + (Double.Parse(axKFOpenAPI1.GetCommData(e.sTrCode, e.sRQName, 0, "고가")) - Double.Parse(axKFOpenAPI1.GetCommData(e.sTrCode, e.sRQName, 0, "저가"))) / 2;
                today_center.Text = day_center_line.ToString();
                today_open.Text = day_open.ToString();
            }
        }

        private void command_form_FormClosed(object sender, FormClosedEventArgs e)
        {
            //conn.Close();
            timer1.Stop();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }


        private void trade_start_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled == false)
            {
                MessageBox.Show("타이머를 시작했습니다");
                timer1.Start();
            }
            else
            {
                MessageBox.Show("타이머를 멈췄습니다");
                timer1.Stop();
            }
            // 현재가 실시간 조회
            axKFOpenAPI1.SetInputValue("종목코드", symbol_text.Text);
            int result = axKFOpenAPI1.CommRqData("해외선물시세", "opt10001", "", "7777");
            if (result == 0)
                DebugLog_listBox.Items.Add("실시간 가격 조회 성공");
            else
                DebugLog_listBox.Items.Add("실시간 가격 조회 실패");
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void axKFOpenAPI1_OnReceiveChejanData(object sender, AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveChejanDataEvent e)
        {
            // 체결 가격 가져오는 것 this.axKFOpenAPI1.GetChejanData(910)
            /*
             * this._Log("주문체결통보");
                   this._Log("주문번호 : " + KiAPI.GetChejanData(9203));
                   this._Log("종목코드 : " + KiAPI.GetChejanData(9201));
                   this._Log("주문상태 : " + KiAPI.GetChejanData(913));
                   this._Log("종목명 : " + KiAPI.GetChejanData(302));
                   this._Log("주문수량 : " + KiAPI.GetChejanData(900));
                   this._Log("주문가격 : " + KiAPI.GetChejanData(901));
                   this._Log("미체결수량 : " + KiAPI.GetChejanData(902));
                   this._Log("체결누계금액 : " + KiAPI.GetChejanData(903));
                   this._Log("원주문번호 : " + KiAPI.GetChejanData(904));
                   this._Log("주문구분 : " + KiAPI.GetChejanData(905));
                   this._Log("매매구분 : " + KiAPI.GetChejanData(906));
                   this._Log("매도수구분 : " + KiAPI.GetChejanData(907));
                   this._Log("주문/체결시간 : " + KiAPI.GetChejanData(909));
                   this._Log("체결번호 : " + KiAPI.GetChejanData(909));
                   this._Log("체결가 : " + KiAPI.GetChejanData(910));
                   this._Log("체결량 : " + KiAPI.GetChejanData(911));
                   this._Log("원주문번호 : " + KiAPI.GetChejanData(904));
             * 이런식으로 쓰면된다.
             */
            var obj = axKFOpenAPI1.GetChejanData(910);
            DebugLog_listBox2.Items.Add(obj.ToString());
            //trade_price = Double.Parse();
        }

        private void test_btn_Click(object sender, EventArgs e)
        {

            /*
                public const int OP_ERR_NONE = 0; //"정상처리" 
                public const int OP_ERR_NO_LOGIN = -1; //"미접속상태"	
                public const int OP_ERR_CONNECT = -101; //"서버 접속 실패" 
                public const int OP_ERR_VERSION = -102; //"버전처리가 실패하였습니다. 
                public const int OP_ERR_TRCODE = -103; //"TrCode가 존재하지 않습니다.”
                public const int OP_ERR_SISE_OVERFLOW = -200; //”시세과부하”
                public const int OP_ERR_ORDER_OVERFLOW = -201; //”주문과부하” 
                public const int OP_ERR_RQ_WRONG_INPUT = -202; //”조회입력값 오류”
                public const int OP_ERR_ORD_WRONG_INPUT = -300; //”주문입력값 오류”
                public const int OP_ERR_ORD_WRONG_ACCPWD = -301; //”계좌비밀번호를 입력하십시오.”
                public const int OP_ERR_ORD_WRONG_ACCNO = -302; //”타인 계좌를 사용할 수 없습니다.”
                public const int OP_ERR_ORD_WRONG_QTY200 = -303; //”경고-주문수량 200개 초과”
                public const int OP_ERR_ORD_WRONG_QTY400 = -304; //”제한-주문수량 400개 초과.”
             */

            int ret = axKFOpenAPI1.SendOrder("선물옵션현재가", "1000", this.account_number, 2, symbol_text.Text, int.Parse(contract_mount_text.Text), "0", "0", "1", "");
            Thread.Sleep(1000);
            ret = axKFOpenAPI1.SendOrder("선물옵션현재가", "1000", this.account_number, 1, symbol_text.Text, int.Parse(contract_mount_text.Text), "0", "0", "1", "");
        }

        private void axKFOpenAPI1_OnReceiveMsg(object sender, AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveMsgEvent e)
        {
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string symbol = symbol_text.Text;
            int contract_mount = Int32.Parse(contract_mount_text.Text);
            string tick_count = (chart_tick_count_text.Text);

            axKFOpenAPI1.SetInputValue("종목코드", symbol);
            axKFOpenAPI1.SetInputValue("조회일자", DateTime.Now.ToString("yyyyMd"));

            int result = 0;
            result = axKFOpenAPI1.CommRqData("해외선물옵션일차트조회", "opc10003", "", "2");
            //if (result == 0)
            //    DebugLog_listBox.Items.Add("일봉 조회 성공");
            //else
            //    DebugLog_listBox.Items.Add("일봉 조회 실패");


            axKFOpenAPI1.SetInputValue("종목코드", symbol);
            axKFOpenAPI1.SetInputValue("시간단위", tick_count);
            result = axKFOpenAPI1.CommRqData("해외선물옵션틱차트조회", "opc10001", "", "1");
            //if (result == 0)
            //    DebugLog_listBox.Items.Add("틱차트 조회 성공");
            //else
            //    DebugLog_listBox.Items.Add("틱차트 조회 실패");

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void debuglog_groupbox_Enter(object sender, EventArgs e)
        {

        }
    }




    public static class Constants
    {
        public const string TR_OPT10001 = "opt10001"; //현재가
        public const string TR_OPT10003 = "opt10005"; //관심종목
        public const string TR_OPC10001 = "opc10001"; //틱차트
        public const string TR_OPC10002 = "opc10002"; //분차트
        public const string TR_OPC10003 = "opc10003"; //일차트
        public const string TR_OPC10004 = "opc10004";//주차트
        public const string TR_OPC10005 = "opc10005"; //월차트
        public const string TR_OPW30005 = "opw30005"; //주문체결
        public const string TR_OPW30003 = "opw30003"; //잔고내역

        public const int OP_ERR_NONE = 0; //"정상처리" 
        public const int OP_ERR_NO_LOGIN = -1; //"미접속상태"	
        public const int OP_ERR_CONNECT = -101; //"서버 접속 실패" 
        public const int OP_ERR_VERSION = -102; //"버전처리가 실패하였습니다. 
        public const int OP_ERR_TRCODE = -103; //"TrCode가 존재하지 않습니다.”
        public const int OP_ERR_SISE_OVERFLOW = -200; //”시세과부하”
        public const int OP_ERR_ORDER_OVERFLOW = -201; //”주문과부하” 
        public const int OP_ERR_RQ_WRONG_INPUT = -202; //”조회입력값 오류”
        public const int OP_ERR_ORD_WRONG_INPUT = -300; //”주문입력값 오류”
        public const int OP_ERR_ORD_WRONG_ACCPWD = -301; //”계좌비밀번호를 입력하십시오.”
        public const int OP_ERR_ORD_WRONG_ACCNO = -302; //”타인 계좌를 사용할 수 없습니다.”
        public const int OP_ERR_ORD_WRONG_QTY200 = -303; //”경고-주문수량 200개 초과”
        public const int OP_ERR_ORD_WRONG_QTY400 = -304; //”제한-주문수량 400개 초과.”

        public const int DT_NONE = 0;		// 기본문자 형식
        public const int DT_DATE = 1;		// 일자 형식
        public const int DT_TIME = 2;		// 시간 형식
        public const int DT_NUMBER = 3;		// 콤마 숫자 형식
        public const int DT_ZERO_NUMBER = 4;		// 콤마 숫자(0표시) 형식
        public const int DT_SIGN = 5;		// 대비기호 형식
        public const int DT_NUMBER_NOCOMMA = 6;		// 콤마없는 숫자 형식
        public const int DT_ORDREG_GB = 7;		// 접수구분(1:접수, 2:확인, 3:체결, X:거부)
        public const int DT_ORDTYPE = 8;		// 주문유형(1:시장가, 2:지정가, 3:STOP, 4:STOPLIMIT)
        public const int DT_DOUBLE = 9;		// 실수값 처리( 소수점 2자리까지)
        public const int DT_ORDGUBUN = 10;		// 주문구분(1:매도, 2:매수)
        public const int DT_PRICE = 11;		// 가격표시

        public const int DT_LEFT = 16;
        public const int DT_RIGHT = 64;
        public const int DT_CENTER = 32;

        public const int UM_SCREEN_CLOSE = 1000;

    }
}